# README #

### What is this? ###

Useful scripts to do repeating tasks in command line

### How do I get set up? ###

* Install python and configure it into your PATH
* Modify file ```local.json``` with your system values
* Modify file ```lib.py``` variable ```exec_path``` to your execute path
* Run (on the src directory of this project):

    ```installscripts.py```

### How does one use it? ###

Here is the list of commands and their arguments (listed in <*> and if <*> contains '/' then there are alternative options) and what they are supposed to be doing

* **clear** <all/app name> - clears application data; same as executing "adb -s <device_id> shell pm clear <app_package>"
* **cmd** <command> - executes some command in your execute directory
* **connect** [optional]<connectable device IP ending; e.g. "115"> - copys connect command to your clipboard (same as executing "adb -s shell <device id>") OR if optional arg is provided executes command "adb connect <your_ip_start>.<provided_arg>"
* **installapps** [optional]<directory> - finds all ".apk" ending files in current directory (the directory this command is executed) or provided directory and installs them. Note that apk files are searched recursively, so one could only provide root dir and it will find all apks starting from that directory tree node. Same as executing "adb install -r <apk>" on all found apks.
* **ip** - displays and copys to clipboard your IP
* **ls** - executes "ls" command in your execute folder - useful if you forget what can be executed
* **radb** - restarts adb; same as executing "adb kill-server" and "adb start-server"
* **reboot** - reboots selected device; same as executing "adb -s <device_id> shell reboot"
* **screencap** [optional]-k - takes screenshot from selected device and displays it; if arg "-k" is provided then keeps the taken picture, if it is not provided then deletes screenshot file after closing it in picture viewer
* **start** <all/app name> - starts application; if "all" is provided in arguments then starts all defined apps; same as executing "adb -s <device_id> shell am start <app_package_name>"
* **stop** <all/app name> - same as start but stops application; same as executing "adb -s <device_id> shell am force-stop <app_package_name>"
* **uninstallapps** [optional]-f - uninstalls all defined apps; if "-f" is provided then tries to force uninstall them (by deleting apk files)

### Useful tips ###

Put your run directory into PATH

Save into your run directory file named ```do.bat``` (or whatever you like) with the following contents (also modified to your system requirements):

    @echo off
    python C:/tools/%*

And then one can run any of the scripts with the following pattern:

    do <cmd> <args>

### Contribution guidelines ###

File bugs under "Issues"