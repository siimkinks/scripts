#!/usr/bin/env python

import sys, os
import lib


def main(argv):
	props = lib.loadLocalProperties()
	for f in os.listdir(props["SCRIPT_RUNTIME_DIR"]):
		print f


if __name__ == '__main__':
	sys.exit(main(sys.argv))
