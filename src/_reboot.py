#!/usr/bin/env python

import sys
import lib

def main(argv):
    deviceCmd = lib.getDeviceCmd() + "reboot"
    print lib.execCmd(deviceCmd)[0]
        
if __name__ == '__main__':
    sys.exit(main(sys.argv))