#!/usr/bin/env python

import sys, os
import subprocess

dirPrefix = "drawable-"
outputDirs = ["xxhdpi", "xhdpi", "hdpi", "mdpi"]
zooms = ["3", "2", "1.5", "1"]

def main(argv):
    createDirs()
    for f in os.listdir("."):
        if f.endswith(".svg"):
            convert(f)

def convert(inFile):
    for i in range(0, len(outputDirs)):
        outPath = dirPrefix + outputDirs[i] + "/"
        cmd = "rsvg-convert --zoom=" + zooms[i] + " -a --format=png -o " + outPath + inFile[:inFile.index(".svg")] + ".png " + inFile
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
        (output, err) = p.communicate()
        p_status = p.wait()
        print outPath + inFile, "success=", (p_status == 0)

def createDirs():
    for dirSuffix in outputDirs:
        dir = dirPrefix + dirSuffix
        if not os.path.exists(dir):
            os.makedirs(dir)                

if __name__ == '__main__':
    sys.exit(main(sys.argv))