#!/usr/bin/env python

import sys, os
import subprocess
import lib

def main(argv):
    deviceCmd = lib.getDeviceCmd()
    srcDir = "."
    if len(argv) > 1 and argv[1]:
        srcDir = argv[1]
    files = [x for x in os.listdir(srcDir) if x.endswith(".apk")]
    for f in files:
        print "\n-- installing ", f, "\n", installPackage(deviceCmd, f)

def installPackage(deviceCmd, package):
    uninstallCmd = deviceCmd + " install -r " + package
    p = subprocess.Popen(uninstallCmd, stdout=subprocess.PIPE, shell=True)
    (output, err) = p.communicate()
    return output.strip()

if __name__ == '__main__':
    sys.exit(main(sys.argv))