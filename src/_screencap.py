#!/usr/bin/env python

import sys, argparse
import lib


def main(argv):
	props = lib.loadLocalProperties()
	parser = argparse.ArgumentParser()
	parser.add_argument('-k', action='store_true', help="to keep screenshot")
	args = parser.parse_args()

	screencapDevicePath = "/sdcard/screenshot.png"
	screencapLocalPath = props["SCREENCAP_SAVE_FILE"]
	deviceCmd = lib.getDeviceCmd()
	cmd = "{0} shell su -c \"/system/bin/screencap -p {1}\"".format(deviceCmd, screencapDevicePath)
	print lib.execCmd(cmd)[0]

	cmd = "{0} pull {1} {2}".format(deviceCmd, screencapDevicePath, screencapLocalPath)
	print lib.execCmd(cmd)[0]

	cmd = "{0} shell rm {1}".format(deviceCmd, screencapDevicePath)
	print lib.execCmd(cmd)[0]

	cmd = "rundll32 \"%ProgramFiles%/Windows Photo Viewer/PhotoViewer.dll\", ImageView_Fullscreen {0}".format(
		screencapLocalPath)
	print lib.execCmd(cmd)[0]

	if not args.k:
		cmd = "rm {0}".format(screencapLocalPath)
		print lib.execCmd(cmd)[0]

	print "*** FINISHED ***"


if __name__ == '__main__':
	sys.exit(main(sys.argv))
