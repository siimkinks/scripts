#!/usr/bin/env python

import sys, os
import lib

def main(argv):
    props = lib.loadLocalProperties()
    cmd = [argv[1]]
    cmd.append(props["SCRIPT_RUNTIME_DIR"] + "/")
    cmd.extend(argv[2:])
    print lib.execCmd(cmd)[0]

if __name__ == '__main__':
    sys.exit(main(sys.argv))