#!/usr/bin/env python

import sys
import lib
import socket


def main(argv):
	if len(argv) > 1:
		requested_connect = argv[1]
		ip = socket.gethostbyname(socket.gethostname())
		split_ip = str(ip).split('.')[:-1]
		split_ip.append(requested_connect)
		lib.execCmd("adb connect " + '.'.join(split_ip))
	else:
		deviceCmd = lib.getDeviceCmd() + "shell"
		lib.execCmd("echo " + deviceCmd + " | clip")


if __name__ == '__main__':
	sys.exit(main(sys.argv))
