#!/usr/bin/env python

import sys
import lib


def main(argv):
	force = False
	if len(argv) > 1 and argv[1].lower() == "-f":
		force = True
	props = lib.loadLocalProperties()
	uninstallPackages = props["UNINSTALL_APPS"]
	deviceCmd = lib.getDeviceCmd()
	for package in uninstallPackages:
		print package, uninstallPackage(deviceCmd, package, force)


def uninstallPackage(deviceCmd, package, force):
	uninstallCmd = deviceCmd + " uninstall " + package
	output = lib.execCmd(uninstallCmd, False)[0]
	success = output.startswith("Success")
	if not success and force:
		print "\nUninstall unsuccessful trying force remove"
		installPath = lib.execCmd(deviceCmd + " shell pm path %s" % package, False)[0].strip()[8:]
		if installPath.strip():
			uninstallCmd = deviceCmd + " shell \"su -c 'mount -o remount,rw /system; rm %s; echo $?'\"" % installPath
			output = lib.execCmd(uninstallCmd, False)[0]
			return output.strip().endswith("0")
	return success


if __name__ == '__main__':
	sys.exit(main(sys.argv))
