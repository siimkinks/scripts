#!/usr/bin/env python

import sys, os
import subprocess
import lib


def main(argv):
	props = lib.loadLocalProperties()
	destDir = props["SCRIPT_RUNTIME_DIR"]
	files = [x for x in os.listdir(".") if x.endswith(".py") and x.startswith("_") and not x.startswith("__")]
	for f in files:
		cpCmd = getCpCmd(f, destDir)
		(output, err, p_status) = lib.execCmd(cpCmd)
		print output
	(output, err, p_status) = lib.execCmd("cp lib.py " + destDir + "/")
	print output
	(output, err, p_status) = lib.execCmd("cp local.json " + destDir + "/")
	print output


def getCpCmd(fileName, destDir):
	fileNameWOEnding = fileName[1:-3]
	return "cp {0} {1}/{2}".format(fileName, destDir, fileNameWOEnding)


if __name__ == '__main__':
	sys.exit(main(sys.argv))
