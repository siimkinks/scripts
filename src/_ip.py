#!/usr/bin/env python

import sys
import lib
import socket


def main(argv):
	ip = socket.gethostbyname(socket.gethostname())
	print "Your IP is:\n", ip, "\n\n"
	lib.execCmd("echo " + ip + " | clip")


if __name__ == '__main__':
	sys.exit(main(sys.argv))
