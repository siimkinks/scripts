#!/usr/bin/env python

import sys
import lib

def main(argv):
    print lib.execCmd("adb kill-server")[0]
    print lib.execCmd("adb start-server")[0]
    print "*** FINISHED ***"
        
if __name__ == '__main__':
    sys.exit(main(sys.argv))