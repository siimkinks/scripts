#!/usr/bin/env python

import subprocess
import json

exec_path = "C:/tools/"

def loadLocalProperties():
	with open(exec_path + 'local.json', 'r') as data_file:
		return json.load(data_file)


def execCmd(cmd, printCmd=True):
	if not isinstance(cmd, basestring):
		cmd = ' '.join(cmd)
	if printCmd:
		print "executing command", cmd
	p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
	(output, err) = p.communicate()
	p_status = p.wait()
	return (output, err, p_status)


def getDeviceCmd():
	device = getDeviceIfNeeded()
	deviceCmd = "adb "
	if device is not None:
		deviceCmd = "adb -s " + device + " "
	return deviceCmd


def getDeviceIfNeeded():
	p = subprocess.Popen("adb devices", stdout=subprocess.PIPE, shell=True)
	(output, err) = p.communicate()
	devices = filter(None, output.split("\r\n")[1:])
	devices = [x.split("\t")[0] for x in devices if not x.startswith("*") and not x.startswith("List")]
	if len(devices) > 1:
		print "Select device:"
		for i in range(0, len(devices)):
			print i + 1, devices[i]
		deviceNr = int(raw_input().strip()) - 1
		return devices[deviceNr]
	return None
