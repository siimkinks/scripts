#!/usr/bin/env python

import sys, os
import re


def main(argv):
	if len(argv) > 1:
		path = argv[1]
	else:
		path = "."
	path += "/"
	print "Correcting words in path", path
	for fileName in os.listdir(path):
		newName = fileName.replace(" ", "_")
		newName = newName.lower()
		splitFileName = newName.split(".")
		fileFirstNamePart = splitFileName[0]
		if fileFirstNamePart.endswith("_v"):
			newName = "v_" + fileFirstNamePart[:len(fileFirstNamePart) - 2] + "." + '.'.join(
				[name for name in splitFileName[1:]])
		if fileName != newName:
			print "renaming", fileName, "to", newName
			os.rename(path + fileName, path + newName)
		matchObj = re.match("^[a-z0-9_.]+$", newName)
		if matchObj == None:
			print "***** word\"", fileName, "\"contains illegal characters. Fix it you must! Accepted are alphanumerals, \"_\" and \".\""


if __name__ == '__main__':
	sys.exit(main(sys.argv))
