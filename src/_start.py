#!/usr/bin/env python

import sys, os
import subprocess
import lib


def main(argv):
	props = lib.loadLocalProperties()
	apps = props["START_APPS"]
	deviceCmd = lib.getDeviceCmd()
	if len(argv) > 1:
		userInput = argv[1].lower()
		if userInput == "all":
			for app in apps.itervalues():
				print app, "\n", startPackage(deviceCmd, app)
		elif userInput in apps:
			appStartPackage = apps[userInput]
			print appStartPackage, "\n", startPackage(deviceCmd, appStartPackage)
		else:
			print "no such app \"" + userInput + "\""


def startPackage(deviceCmd, package):
	uninstallCmd = deviceCmd + " shell \" am start " + package + "\""
	p = subprocess.Popen(uninstallCmd, stdout=subprocess.PIPE, shell=True)
	(output, err) = p.communicate()
	return output


if __name__ == '__main__':
	sys.exit(main(sys.argv))
